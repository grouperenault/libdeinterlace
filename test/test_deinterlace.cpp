/*
 * Copyright (c) 2020 Renault S.A.S.
 * This Source Code Form is subject to the terms of the Mozilla Public * License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * you can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include "deinterlace.h"

const int nbImages=5;
const int width=9;
const int halfFrameHeight=4;
const int fullFrameHeight=halfFrameHeight*2-1;
const int stride=16;

uint32_t imageSlow[nbImages][halfFrameHeight][stride]={
    { {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000}}
};

uint32_t imageFast[nbImages][halfFrameHeight][stride]={
    { {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF},
      {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF}}
};

uint32_t imageNoise[nbImages][halfFrameHeight][stride]={
    { {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x40404040, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000}},
    { {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x40404040, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    { {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x40404040, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000},
      {0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000}},
    { {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x40404040, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x40404040, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000},
      {0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000, 0x00000000}}
};

uint8_t outSlow[2][fullFrameHeight][width+1]={
  { "..XXX....",
    "..XXX....",
    "..XXX....",
    "..XXX....",
    "..XXX....",
    "..XXX....",
    "..XXX...."},
  { "...XXX...",
    "...XXX...",
    "...XXX...",
    "...XXX...",
    "...XXX...",
    "...XXX...",
    "...XXX..."}
};

uint8_t outFast[2][fullFrameHeight][width+1]={
  { "....X....",
    "....X....",
    "....X....",
    "....X....",
    "....X....",
    "....X....",
    "....X...."},
  { "......X..",
    "......X..",
    "......X..",
    "......X..",
    "......X..",
    "......X..",
    "......X.."}
};

uint8_t outNoise[2][fullFrameHeight][width+1]={
  { "X...X....",
    "..X...X..",
    "X.o.X....",
    "o.X.o.X..",
    "X...X....",
    "..X...X..",
    "X...X...."},
  { "X...X....",
    "..X...X..",
    "X.o.X.o..",
    "..X.o.X..",
    "X.o.X....",
    "..X...X..",
    "X...X...."}
};

uint32_t imageOut[fullFrameHeight*stride+1]={};		// +1: to check buffer overflow
#if defined (__ARM_NEON)
uint32_t imageOutNeon[fullFrameHeight*stride+1]={};		// +1: to check buffer overflow
#endif

uint8_t pixelToChar(uint32_t pixel)
{
  static const char c[]=".oO0X";
  int lum = pixelLum(pixel);
  return (c[(lum+32)>>6]);
}

void printImage(uint32_t *pixel, int width, int height, int stride)
{
    for (int i=0 ; i<height ; i++) {
        for (int j=0 ; j<width ; j++) {
            printf("%c", pixelToChar(pixel[j]));
        }
        printf("\n");
        pixel+=stride;
    }
}

int checkImage(uint32_t *pixel, uint8_t outRef[fullFrameHeight][width+1], int width, int height, int stride)
{
    int ret = 0;
    for (int i=0 ; i<height ; i++) {
        for (int j=0 ; j<width ; j++) {
            if (pixelToChar(pixel[j]) != outRef[i][j]) {
                printf("Output Image differs at pixel[%d,%d] '%c' != '%c'\n", j, i, pixelToChar(pixel[j]), outRef[i][j]);
                ret = -1;
            }
        }
        pixel+=stride;
    }
    return ret;
}

int test(uint32_t image[nbImages][halfFrameHeight][stride], uint8_t outRef[2][fullFrameHeight][width+1], const char *testName)
{
    int ret = 0;
    for (int i=0 ; i<2 ; i++) {
        uint32_t *image0Ptr = image[i][0];
        uint32_t *image1Ptr = image[i+1][0];
        uint32_t *image2Ptr = image[i+2][0];
        uint32_t *image3Ptr = image[i+3][0];

        weave(image0Ptr, image1Ptr, image2Ptr, image3Ptr, halfFrameHeight, width, stride, stride, imageOut, i%2);
        printf("%s weave %s:\n", testName, (i%2) ? "odd" : "even");
        printImage(imageOut, width, fullFrameHeight, stride);

		    imageOut[fullFrameHeight*stride]=0xEBEBEBEB;
        deinterlace(image0Ptr, image1Ptr, image2Ptr, image3Ptr, halfFrameHeight, width, stride, stride, imageOut, i%2);
        printf("%s deinterlace %s:\n", testName, (i%2) ? "odd" : "even");
        printImage(imageOut, width, fullFrameHeight, stride);
        if (imageOut[fullFrameHeight*stride] != 0xEBEBEBEB) {
              printf("Buffer overflow!!!\n");
              ret = -1;
        }
        ret |= checkImage(imageOut, outRef[i], width, fullFrameHeight, stride);

#if defined (__ARM_NEON)
        imageOut[fullFrameHeight*stride]=0xEBEBEBEB;
        deinterlaceNeon16(image0Ptr, image1Ptr, image2Ptr, image3Ptr, halfFrameHeight, width, stride, stride, imageOutNeon, i%2);
        printf("%s deinterlaceNeon16 %s:\n", testName, (i%2) ? "odd" : "even");
        printImage(imageOutNeon, width, fullFrameHeight, stride);
        if (imageOut[fullFrameHeight*stride] != 0xEBEBEBEB) {
              printf("Buffer overflow!!!\n");
              ret = -1;
        }
        ret |= checkImage(imageOut, outRef[i], width, fullFrameHeight, stride);
#endif
        printf("\n");
    }
    return (ret);
}

int main(int argc, char** argv)
{
    int ret = 0;
    ret |= test(imageSlow, outSlow, "Slow Motion");
    ret |= test(imageFast, outFast, "Fast Motion");
    ret |= test(imageNoise, outNoise, "Noise");

    return(ret);
}
