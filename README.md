[![coverage report](https://gitlab.com/grouperenault/libdeinterlace/badges/master/coverage.svg)](https://gitlab.com/grouperenault/libdeinterlace/-/commits/master)

# Release blog post

https://medium.com/grouperenault/get-the-most-of-analog-camera-in-the-automotive-context-7a5776220a12

# De-interlace Filter

De-interface Filter is a CPP library for de-interlacing Analog camera frames.

This filter is suitable for Automotive Analog Camera, NTSC BT-656 720x507i@60.

It is light enough to be embedded in In-Vehicule Infotainment systems and there is a vectored version using Armv8/Neon.

## Algo description

The algo tries to address 3 topics:

- keep the best resolution, main drawback of Frame Drop algo.
- avoid visual artifacts, like flickering with Bob algo, or mice teeth with Weave algo.
- consume a reasonable amount of CPU and memory to be compliant with entry IVI systems, main drawback of Yadif algo.

The adopted solution is based on motion detection:
- If an area is moving, we will use spacial interpolation (to avoid artifacts)
- If an area is not moving, we will use temporal interpolation (to get best resolution)

The motion detection is evaluated regarding pixel luminosity variation between fields of the same parity (t-2 & t, t-1 & t+1).

The algo is applied on pixels of new field (t+1), using the up and down pixels of previous field (t) for spacial interpolation, and previous pixel of same parity (t-1) for temporal interpolation. Left and right pixels are not used.

It can be sum-up as:

- if Up pixel and Down pixel are moving: use spacial interpolation between Up & Down pixels: this will avoid Mice teeth artifacts.
- if Up pixel or Down pixel or Local pixel are moving: use spacial interpolation between Up & Down pixels, bounded by previous and next pixels values: this will avoid spark artifacts due to CCD noise amplification resulting of previous case.
- if none of around pixels are moving, use temporal interpolation between Previous and Next pixels: this will keep the best resolution.

Look at *doc/DeinterlaceAlgo.xlsx* for a more graphical description.

## Preview

Please have a look at these sample videos to get a good idea of the algo:

![Reference Video](test/sample.mp4)
![Interlaced Video](test/sample_i.mp4)
![De-interlaced Video](test/sample_di.mp4)

You can compare the result with other algo provided by **GStreamer**, among others:

![Tomsmocomp algo](test/sample_tomsmocomp.mp4)
![Scale Bob algo](test/sample_scale_bob.mp4)
![Yadif algo](test/sample_yadif.mp4)

## Performances

For a 720x507i@60 camera stream, running on a 2.4GHz Armv8 CPU:
- Arm version: 8% load of 1 core
- Neon version: 2% load of 1 core

## Interface
```C
#include "deinterlace.h"

int deinterlace(uint32_t* previous2BufPtr,
                uint32_t* previousBufPtr,
                uint32_t* currentBufPtr,
                uint32_t* nextBufPtr,
                int       inHeight,
                int       inWidth,
                int       inStride,
                int       outStride,
                uint32_t* outBufPtr,
                bool      currentBufField);

int deinterlaceNeon16(uint32_t* previous2BufPtr,
                      uint32_t* previousBufPtr,
                      uint32_t* currentBufPtr,
                      uint32_t* nextBufPtr,
                      int       inHeight,
                      int       inWidth,
                      int       inStride,
                      int       outStride,
                      uint32_t* outBufPtr,
                      bool      currentBufField);
```

Image format is RGBA32 Little Endian. Input buffers contain fields (even/odd), output buffer contains frame at same frame rate (even+odd, odd+even).

- currentBufPtr: contains the reference field that will be copied into the output frame as it is.
- nextBufPtr: contains the next field that will be mixed de-interlaced with the reference field into the output frame.
- outBufPtr: will contain the output frame containing both fields de-interlaced.
- inStride: global size of an input raw in pixels (could be >= effective frame width), must be %16 for Neon version.
- outStride: global size of an output raw in pixels.
- currentBufField: parity of current Buffer: 0 for even, 1 for odd.

## Compilation and test

There is a *test/test_deinterlace.cpp* program that demonstrates the usage and apply the filter on basic streams. You can look at the ouput here: [test/test_deinterlace.out.ref](test/test_deinterlace.out.ref).

### Using makefile
**For Native target:**

To compile the library on local Linux:
```bash
export CC=g++
make test_deinterlace
```
And test with:
```bash
./test_deinterlace
```

**For Arm64 target:**

To cross-compile the library for Arm64 linux:
```bash
sudo apt-get install g++-aarch64-linux-gnu
export CC=aarch64-linux-gnu-g++
make test_deinterlace
```
And test with:
```bash
sudo apt-get install qemu-user-static
qemu-aarch64-static -L /usr/aarch64-linux-gnu test_deinterlace
```

**For Android target:**

To cross-compile the library for Arm64 Android:
Installed the [Android NDK](https://developer.android.com/ndk/downloads), then:
```bash
export NDK=<your Android Sdk/Ndk location>
export CC=${NDK}/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android22-clang
make test_deinterlace
```
And test on an Android device with:
```bash
adb root
adb push test_deinterlace /data
adb shell "/data/test_deinterlace"
```

### Using meson
**For Native target:**

To compile the library on Linux:
```bash
sudo apt-get install meson
meson nativebuild/
cd nativebuild
meson compile
```
And test with:
```bash
meson test
```

**For Arm64 target:**

To cross-compile the library for Arm64 linux:
```bash
sudo apt-get install meson
sudo apt-get install g++-aarch64-linux-gnu
meson aarch64build/ --cross-file aarch64.ini
cd aarch64build
meson compile
```
And test with:
```bash
sudo apt-get install qemu-user-static
qemu-aarch64-static -L /usr/aarch64-linux-gnu test_deinterlace
```

### Using Android build system
You can also embed the library in your Android build, thanks to the provided *Android.bp*.

## Demo

There is a [test/demo_deinterlace.cpp](test/demo_deinterlace.cpp) program that can be used in a **Gstreamer** pipeline to de-interlace raw frames.

To compile it:
```bash
export CC=g++
make demo_deinterlace
```
To use it on a video sample:
```bash
sudo apt-get install gstreamer1.0-x gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad  gstreamer1.0-plugins-ugly gstreamer1.0-libav
cd test
./gstreamer.sh sample.MOV
```
3 videos will be generated:

 - [sample.mp4](test/sample.mp4) that contains the reference video
 - [sample_i.mp4](test/sample_i.mp4) that contains the interlaced video
 - [sample_di.mp4](test/sample_di.mp4) that contains the de-interlaced video

For comparison purpose, it will also generate de-interlaced videos with all other algo provided by Gstreamer, among others: **Yadif**.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MPLv2](LICENSE.md)
