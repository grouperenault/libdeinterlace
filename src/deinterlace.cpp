/*
 * Copyright (c) 2020 Renault S.A.S.
 * This Source Code Form is subject to the terms of the Mozilla Public * License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * you can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <string.h>

#include "deinterlace.h"

#define GREEN_PIXEL 0xff00ff00
#define RED_PIXEL   0xff0000ff
#define BLUE_PIXEL  0xffff0000

// Enable this to highlight impacted pixels
//#define COLORED

// Enable this to identify functions impacting performances (see simpleperf tool)
#define INLINE __attribute__( ( noinline ) )
//#define INLINE  //__attribute__( ( always_inline ) )

// Threshold for movement detection [0-255]
#define DELTA_LUM_THRESHOLD 20

int clamp(int v, int min, int max)  INLINE;
int clamp(int v, int min, int max) {
    if (v < min) return min;
    if (v > max) return max;
    return v;
}

// LCOV_EXCL_START
uint32_t yuvToRgbx(const unsigned char Y, const unsigned char Uin, const unsigned char Vin) INLINE;
uint32_t yuvToRgbx(const unsigned char Y, const unsigned char Uin, const unsigned char Vin)
{
    float U = Uin - 128.0f;
    float V = Vin - 128.0f;

    float Rf = Y + 1.140f*V;
    float Gf = Y - 0.395f*U - 0.581f*V;
    float Bf = Y + 2.032f*U;
    unsigned char R = (unsigned char)clamp(Rf, 0.0f, 255.0f);
    unsigned char G = (unsigned char)clamp(Gf, 0.0f, 255.0f);
    unsigned char B = (unsigned char)clamp(Bf, 0.0f, 255.0f);

    return ((R & 0xFF))       |
           ((G & 0xFF) << 8)  |
           ((B & 0xFF) << 16) |
           0xFF000000;  // Fill the alpha channel with ones
}

uint32_t rgbxToYuv(uint32_t rgbx) INLINE;
uint32_t rgbxToYuv(uint32_t rgbx)
{
    float R = rgbx & 0xff;
    float G = (rgbx >> 8) & 0xff;
    float B = (rgbx >> 16) & 0xff;

    float Yf = 0.299*R + 0.587*G + 0.114*B;
    float Uf = -0.147*R - 0.288*G + 0.436*B;
    float Vf = 0.615*R - 0.514*G - 0.100*B;

    unsigned char Y = (unsigned char)clamp(Yf, 0.0f, 255.0f);
    unsigned char U = (unsigned char)clamp(Uf, 0.0f, 255.0f);
    unsigned char V = (unsigned char)clamp(Vf, 0.0f, 255.0f);

    return ( (Y & 0xFF)        |
            ((U & 0xFF) << 8)  |
            ((Y & 0xFF) << 16) |
            ((V & 0xFF) << 24));
}

void setPixelLum(uint32_t* rawPixel, int lum)  INLINE;
void setPixelLum(uint32_t* rawPixel, int lum) {
    uint32_t yuv = rgbxToYuv(*rawPixel);

    uint8_t Y = yuv & 0xff;
    uint8_t U = (yuv >> 8) & 0xff;
    uint8_t V = (yuv >> 24) & 0xff;

    Y = lum;

    *rawPixel = yuvToRgbx(Y, U, V);
}
// LCOV_EXCL_STOP

int pixelLum(uint32_t rgbx)  INLINE;
int pixelLum(uint32_t rgbx) {
    int R = rgbx & 0xfful;
    int G = (rgbx >> 8) & 0xfful;
    int B = (rgbx >> 16) & 0xfful;

    int Y = ( ((int)(0.299*256))*R + ((int)(0.587*256))*G + ((int)(0.114*256))*B ) >> 8;

    int lum = clamp(Y, 0, 255);

    return lum;
}

uint32_t pixelMean(uint32_t rawPixel1, uint32_t rawPixel2)  INLINE;
uint32_t pixelMean(uint32_t rawPixel1, uint32_t rawPixel2) {
    uint8_t* pixel1 = (uint8_t*) &rawPixel1;
    uint8_t* pixel2 = (uint8_t*) &rawPixel2;
    uint32_t meanRawPixel;
    uint8_t* meanPixel = (uint8_t*) &meanRawPixel;

    meanPixel[0] = (pixel1[0] + pixel2[0]) >> 1;    // R
    meanPixel[1] = (pixel1[1] + pixel2[1]) >> 1;    // G
    meanPixel[2] = (pixel1[2] + pixel2[2]) >> 1;    // B
    meanPixel[3] = (pixel1[3] + pixel2[3]) >> 1;    // A

    return meanRawPixel;
}

bool pixelMovement(uint32_t rawPixel1, uint32_t rawPixel2) INLINE;
bool pixelMovement(uint32_t rawPixel1, uint32_t rawPixel2) {
    int dR = abs((int)((rawPixel1 & 0xfful) - (rawPixel2 & 0xfful)));
    int dG = abs((int)(((rawPixel1 >> 8) & 0xfful) - ((rawPixel2 >> 8) & 0xfful)));
    int dB = abs((int)(((rawPixel1 >> 16) & 0xfful) - ((rawPixel2 >> 16) & 0xfful)));

    int dY = ( ((int)(0.299*256))*dR + ((int)(0.587*256))*dG + ((int)(0.114*256))*dB ) >> 8;

    bool movement = (dY >= DELTA_LUM_THRESHOLD);

    return movement;
}

uint32_t pixelBound(int lum1, int lum2, int lum, uint32_t pixel1, uint32_t pixel2, uint32_t pixel) INLINE;
uint32_t pixelBound(int lum1, int lum2, int lum, uint32_t pixel1, uint32_t pixel2, uint32_t pixel)
{
    uint32_t boundedPixel;
    int maxLum, minLum;
    uint32_t minPixel, maxPixel;

    if (lum1 > lum2) {
        maxLum = lum1;
        maxPixel = pixel1;
        minLum = lum2;
        minPixel = pixel2;
    }
    else {
        maxLum = lum2;
        maxPixel = pixel2;
        minLum = lum1;
        minPixel = pixel1;
    }
    
    if (lum > maxLum) {
        boundedPixel = maxPixel;
        //printf("pixelBound max lum1:%d lum2:%d minLum:%d maxLum:%d lum:%d pixelLum:%d!\n", lum1, lum2, minLum, maxLum, lum, pixelLum(boundedPixel));
    }
    else if (lum < minLum) {
        boundedPixel = minPixel;
        //printf("pixelBound max lum1:%d lum2:%d minLum:%d maxLum:%d lum:%d pixelLum:%d!\n", lum1, lum2, minLum, maxLum, lum, pixelLum(boundedPixel));
    }
    else {
        boundedPixel = pixel;
    }
    return boundedPixel;
}

int deinterlace(uint32_t* previous2Buf, uint32_t* previousBuf, uint32_t* currentBuf, uint32_t* nextBuf, int inHeight, int inWidth, int inStride, int outStride, uint32_t* outBuf, bool currentBufField)
{
    int outHeight = inHeight * 2 - 1;

    //printf("height: %d\t width: %d\t stride:%d\n", inHeight, inWidth, stride);

    // Current buf contains Even fields
    if (0 == currentBufField)
    {
        for (int inLine = 0, outLine = 0; inLine < inHeight; inLine++)
        {
            // Even line: copy from current buffer (common for both bob and weave)
            memcpy(&outBuf[outLine * outStride], &currentBuf[inLine * inStride], inWidth * 4);
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }

            // Odd line: let's decide if we rely on odd pixels (weave) or even interpolated ones (Bob)
            for (int px = 0; px < inWidth; px++)
            {
                uint32_t nextPixel = nextBuf[inLine * inStride + px];
                uint32_t previousPixel = previousBuf[inLine * inStride + px];
                uint32_t upPixel = currentBuf[inLine * inStride + px];
                uint32_t downPixel = (inLine < inHeight - 1) ?  currentBuf[(inLine + 1) * inStride + px] :
                                                                currentBuf[inLine * inStride + px];
                uint32_t upPixel2 = previous2Buf[inLine * inStride + px];
                uint32_t downPixel2 = (inLine < inHeight - 1) ? previous2Buf[(inLine + 1) * inStride + px] :
                                                                previous2Buf[inLine * inStride + px];

                // Even movement?
                bool nextMove = pixelMovement(nextPixel, previousPixel);
                // Odd movement?
                bool upMove = pixelMovement(upPixel, upPixel2);
                bool downMove = pixelMovement(downPixel, downPixel2);

                if (upMove && downMove) {
                    // Moving for real: let's rely on spacial pixel approximation to avoid mice teeth artifact
                    uint32_t meanSpacialPixel = pixelMean(upPixel, downPixel);
#ifndef COLORED
                    outBuf[outLine * outStride + px] = meanSpacialPixel;
#else
                    outBuf[outLine * outStride + px] = GREEN_PIXEL;
#endif
                } else if (upMove || downMove || nextMove) {
                    // Something is moving around: let's rely on spacial pixel approximation to avoid mice teeth artifact
                    // But bound lum to next and previous limits to avoid spark effect due to CDD noise.
                    int upLum = pixelLum(upPixel);
                    int downLum = pixelLum(downPixel);
                    int meanSpacialLum = (upLum + downLum) >> 1;
                    int nextLum = pixelLum(nextPixel);
                    int previousLum = pixelLum(previousPixel);
                    uint32_t meanSpacialPixel = pixelMean(upPixel, downPixel);
                    uint32_t boundedSpacialPixel = pixelBound(nextLum, previousLum, meanSpacialLum, nextPixel, previousPixel, meanSpacialPixel);
#ifndef COLORED
                    outBuf[outLine * outStride + px] = boundedSpacialPixel;
#else
                    outBuf[outLine * outStride + px] = RED_PIXEL;
#endif
                } else {
                    // Everything seams calm, let's rely on temporal pixel approximation
                    uint32_t meanTemporalPixel = pixelMean(nextPixel, previousPixel);
                    outBuf[outLine * outStride + px] = meanTemporalPixel;
                }
            }
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }
        }
    }
    // Current buf contains Odd fields
    else
    {
        for (int inLine = 0, outLine = 0; inLine < inHeight; inLine++)
        {
            // Even line: let's decide if we rely on even pixels (weave) or odd interpolated ones (Bob)
            for (int px = 0; px < inWidth; px++)
            {
                uint32_t nextPixel = nextBuf[inLine * inStride + px];
                uint32_t previousPixel = previousBuf[inLine * inStride + px];
                uint32_t upPixel = (inLine > 0) ?   currentBuf[(inLine - 1) * inStride + px] : 
                                                    currentBuf[inLine * inStride + px];
                uint32_t downPixel = (inLine < inHeight - 1) ?  currentBuf[(inLine) * inStride + px] :
                                                                currentBuf[(inLine - 1) * inStride + px] ;
                uint32_t upPixel2 = (inLine > 0) ?  previous2Buf[(inLine - 1) * inStride + px] : 
                                                    previous2Buf[inLine * inStride + px];
                uint32_t downPixel2 = (inLine < inHeight - 1) ?    previous2Buf[(inLine) * inStride + px] :
                                                                   previous2Buf[(inLine - 1) * inStride + px] ;

                // Odd movement?
                bool nextMove = pixelMovement(nextPixel, previousPixel);
                // Even movement?
                bool upMove = pixelMovement(upPixel, upPixel2);
                bool downMove = pixelMovement(downPixel, downPixel2);

                if (upMove && downMove) {
                    // Moving for real: let's rely on spacial pixel approximation to avoid mice teeth artifact
                    uint32_t meanSpacialPixel = pixelMean(upPixel, downPixel);
#ifndef COLORED 
                    outBuf[outLine * outStride + px] = meanSpacialPixel;
#else
                    outBuf[outLine * outStride + px] = GREEN_PIXEL;
#endif
                } else if (upMove || downMove || nextMove) {
                    // Something is moving around: let's rely on spacial pixel approximation to avoid mice teeth artifact
                    // But bound lum to next and previous limits to avoid spark effect due to CDD noise.
                    int upLum = pixelLum(upPixel);
                    int downLum = pixelLum(downPixel);
                    int meanSpacialLum = (upLum + downLum) >> 1;
                    int nextLum = pixelLum(nextPixel);
                    int previousLum = pixelLum(previousPixel);
                    uint32_t meanSpacialPixel = pixelMean(upPixel, downPixel);
                    uint32_t boundedSpacialPixel = pixelBound(nextLum, previousLum, meanSpacialLum, nextPixel, previousPixel, meanSpacialPixel);
#ifndef COLORED 
                    outBuf[outLine * outStride + px] = boundedSpacialPixel;
#else
                    outBuf[outLine * outStride + px] = RED_PIXEL;
#endif
                } else {
                    // Everything seams calm, let's rely on temporal pixel approximation
                    uint32_t meanTemporalPixel = pixelMean(nextPixel, previousPixel);
                    outBuf[outLine * outStride + px] = meanTemporalPixel;
                }
            }
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }

            // Odd line: copy from current buffer (common for both bob and weave)
            memcpy(&outBuf[outLine * outStride], &currentBuf[inLine * inStride], inWidth * 4);
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }
        }
    }
    return 0;
}

int weave(uint32_t* previous2Buf, uint32_t* previousBuf, uint32_t* currentBuf, uint32_t* nextBuf, int inHeight, int inWidth, int inStride, int outStride, uint32_t* outBuf, bool currentBufField)
{
    int outHeight = inHeight * 2 - 1;

    //printf("height: %d\t width: %d\t stride:%d\n", inHeight, inWidth, stride);

    // Current buf contains Even fields
    if (0 == currentBufField)
    {
        for (int inLine = 0, outLine = 0; inLine < inHeight; inLine++)
        {
            // Even line: copy from current buffer (common for both bob and weave)
            memcpy(&outBuf[outLine * outStride], &currentBuf[inLine * inStride], inWidth * 4);
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }

            // Odd line: copy from next buffer
            memcpy(&outBuf[outLine * outStride], &nextBuf[inLine * inStride], inWidth * 4);
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }
        }
    }
    // Current buf contains Odd fields
    else
    {
        for (int inLine = 0, outLine = 0; inLine < inHeight; inLine++)
        {
            // Even line: copy from next buffer
            memcpy(&outBuf[outLine * outStride], &nextBuf[inLine * inStride], inWidth * 4);
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }

            // Odd line: copy from current buffer (common for both bob and weave)
            memcpy(&outBuf[outLine * outStride], &currentBuf[inLine * inStride], inWidth * 4);
            outLine++;
            if (outLine >= outHeight)
            {
                break;
            }
        }
    }
    return 0;
}
